import pygame
from pygame.locals import *

pygame.init()
fenetre = pygame.display.set_mode((1275, 741))
fond = pygame.image.load('fond_test.png')
tete_serpent = pygame.image.load('tete_serpent.png').convert_alpha()

posy = 50
posx = 50
fenetre.blit(fond,(0,0))
fenetre.blit(tete_serpent, (posx, posy))
pygame.display.flip()

while True:
	for event in pygame.event.get():	
		if event.type == QUIT:
			pygame.quit()
			exit
		if event.type == KEYDOWN:
			if event.key == K_RIGHT:	
				posx += 50
	fenetre.blit(fond,(0,0))
	fenetre.blit(tete_serpent, (posx, posy))
	pygame.display.update()