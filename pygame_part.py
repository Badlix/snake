import pygame
from pygame.locals import *

terrain = [[0,0,0,0,7,0,0,0,0,0],[0,0,1,0,0,0,0,0,0,0],[0,0,2,0,0,0,0,0,0,0],[0,0,2,0,0],[7,0,3,0,0]]
dimension_grille = 15
dimension = int(800/dimension_grille)

pygame.init()
display = pygame.display.set_mode((800,900)) 
fond  = pygame.image.load('images/fond_test.png')
fond = pygame.transform.scale(fond, (800,800))
display.blit(fond,(0,100))
tete = pygame.image.load('images/tete_test.png')
tete = pygame.transform.scale(tete, (dimension,dimension))
corps = pygame.image.load('images/corps_test.png')
corps = pygame.transform.scale(corps, (dimension, dimension))
queue = pygame.image.load('images/queue_test.png')
queue = pygame.transform.scale(queue, (dimension,dimension))
pomme = pygame.image.load('images/pomme_test.png')
pomme = pygame.transform.scale(pomme,(dimension,dimension))
vide = pygame.image.load('images/vide_test2.png')
vide = pygame.transform.scale(vide, (dimension,dimension))

def afficher():
    y = 100
    x = 0
    for ligne in terrain:
        for nombre in ligne:
            if nombre == 0:
                # sprite = pygame.image.load('vide_test.png')
                display.blit(vide, (x,y))
            elif nombre == 1:
                # sprite = pygame.image.load('tete_test.png')
                display.blit(tete, (x,y))
            elif nombre == 2:
                # sprite = pygame.image.load('corps_test.png')
                display.blit(corps, (x,y))
            elif nombre == 3:
                # sprite = pygame.image.load('queue_test.png')
                display.blit(queue, (x,y))
            elif nombre == 7:
                # sprite = pygame.image.load('pomme_test.png')
                display.blit(pomme, (x,y))
            x += (dimension)
        y += (dimension)
        x = 0

afficher()
pygame.display.flip()

while True:
    for event in pygame.event.get():
        if event.type == QUIT:
            pygame.quit()
            exit
    display.blit(fond,(0,0))