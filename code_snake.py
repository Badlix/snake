from random import *
from threading import Thread
import time

terrain_binaire = [] #0 = libre, 1 = tête du serpent, 2 = corp, 3 = queue
dimension = 10
for i in range(dimension):
    terrain_binaire.append([0] * dimension)

fin_du_jeu = False
pomme_manger = True
snake_longeur = 3
deplacement = -1
direction = 3
coordonnees = [] # [y, x]
aleatoire = randint(0,dimension - 1) # defini la position de départ du serpent
coordonnees.append([aleatoire, 2])
coordonnees.append([aleatoire, 1])
coordonnees.append([aleatoire, 0])

class mouvement_continuel(Thread): #fait avancer le snake tt seul
    
    def __init__(self):
        Thread.__init__(self)
    
    def run(self):
        global direction
        global deplacement
        global snake_longeur
        global fin_du_jeu
        while fin_du_jeu == False:
            mettre_pomme()
            time.sleep(1.2)
            terrain_binaire[coordonnees[-1][0]][coordonnees[-1][1]] = 0
            if direction == 1 or direction == 3:
                if coordonnees[0][1] - deplacement > dimension - 1 or coordonnees[0][1] - deplacement < 0:
                    print('\n' + 'PERDU' + '\n')
                    print(1)
                    fin_du_jeu = True
                    break
            if direction == 2 or direction == 5: 
                # print(coordonnees[0][0])   
                if coordonnees[0][0] + deplacement> dimension - 1 or coordonnees[0][0] + deplacement< 0:
                    print('\n' + 'PERDU' + '\n')
                    print(2)
                    fin_du_jeu = True
                    break
            if snake_longeur == len(coordonnees): #si la pomme n'est pas mangé
                del coordonnees[-1]
            if direction == 1 or direction == 3:
                if terrain_binaire[coordonnees[0][0]][coordonnees[0][1] - deplacement] != 0 and terrain_binaire[coordonnees[0][0]][coordonnees[0][1] - deplacement] != 7: #regarde si il y a collision
                    print('\n' + 'PERDU' + '\n')
                    print(3)
                    fin_du_jeu = True
                    break
                coordonnees.insert(0, [coordonnees[0][0], coordonnees[0][1] - deplacement])
            if direction == 2 or direction == 5:
                if terrain_binaire[coordonnees[0][0] + deplacement][coordonnees[0][1]] != 0 and terrain_binaire[coordonnees[0][0] + deplacement][coordonnees[0][1]] != 7: #regarde si il y a collision
                    print('\n' + 'PERDU' + '\n')
                    print(4)
                    fin_du_jeu = True
                    break
                coordonnees.insert(0, [coordonnees[0][0] + deplacement, coordonnees[0][1]])
            update_terrain_binaire()
            # update_superieur()
            afficher_terrain_binaire()
            

def afficher_terrain_binaire():
    print('-----------------------------')
    for i in range(dimension):
        print(terrain_binaire[i])

def update_terrain_binaire():
    global pomme_manger
    global snake_longeur
    for i in range(len(coordonnees)):
        if i == 0:
            if terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] != 7:
                terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 1
            else:
                print('\n' + 'LA POMME A ETE MANGER' + '\n')
                pomme_manger = True
                # terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 1
                terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 1
                snake_longeur += 1
        elif i == len(coordonnees) - 1:
            terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 3
        else:
            terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 2

# def update_superieur():
#     global pomme_manger
#     global snake_longeur
#     for i in range(len(coordonnees)):
#         if i == 0:
#             if terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] != 7:
#                 if direction == 1:
#                     terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 1
#                 elif direction == 2:
#                     terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 2
#                 elif direction == 3:
#                     terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 3
#                 elif direction == 5:
#                     terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 4
#             else:
#                 print('\n' + 'LA POMME A ETE MANGER' + '\n')
#                 pomme_manger = True
#                 snake_longeur += 1
#                 if direction == 1:
#                     terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 1 #tete vers gauche
#                 elif direction == 2:
#                     terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 2 #tete vers bas
#                 elif direction == 3:
#                     terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 3 #tete vers droite
#                 elif direction == 5:
#                     terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 4 #tete vers haut
#         elif i == len(coordonnees) - 1:
#             if coordonnees[i][0] - coordonnees[i-1][0] == 1: 
#                 terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 10 #queue vers haut
#             if coordonnees[i][0] - coordonnees[i-1][0] == -1: 
#                 terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 9 #queue vers bas
#             if coordonnees[i][1] - coordonnees[i-1][1] == 1: 
#                 terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 11 #queue vers gauche
#             if coordonnees[i][1] - coordonnees[i-1][1] == -1: 
#                 terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 8 #queue vers droite
#         else:
#             if coordonnees[i-1][1] - coordonnees[i+1][1] == -2 or coordonnees[i-1][1] - coordonnees[i+1][1] == 2:
#                 terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 5 #corps horizontal
#             elif coordonnees[i-1][0] - coordonnees[i+1][0] == -2 or coordonnees[i-1][0] - coordonnees[i+1][0] == 2:
#                 terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 6 #corps vertical
#             elif coordonnees[i-1][1] - coordonnees[i+1][1] == -1:#bleu
#                 if coordonnees[i-1][0] - coordonnees[i+1][0] == -1:#jaune
#                     if coordonnees[i][0] - coordonnees[i+1][0] == 0:#gris
#                         terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 15
#                     if coordonnees[i][0] - coordonnees[i+1][0] == 1 or coordonnees[i][0] - coordonnees[i+1][0] == -1: #olive/kaki
#                         terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 13
#                 if coordonnees[i-1][0] - coordonnees[i+1][0] == 1:#rouge
#                     if coordonnees[i][0] - coordonnees[i+1][0] == 0:#gris
#                         terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 12
#                     if coordonnees[i][0] - coordonnees[i+1][0] == 1 or coordonnees[i][0] - coordonnees[i+1][0] == -1: #olive/kaki
#                         terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 14
#             elif coordonnees[i-1][1] - coordonnees[i+1][1] == 1:#vert
#                 if coordonnees[i-1][0] - coordonnees[i+1][0] == -1:#jaune
#                     if coordonnees[i][0] - coordonnees[i+1][0] == 0:#gris
#                         terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 14
#                     if coordonnees[i][0] - coordonnees[i+1][0] == 1 or coordonnees[i][0] - coordonnees[i+1][0] == -1: #olive/kaki
#                         terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 12
#                 if coordonnees[i-1][0] - coordonnees[i+1][0] == 1:#rouge
#                     if coordonnees[i][0] - coordonnees[i+1][0] == 0:#gris
#                         terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 13
#                     if coordonnees[i][0] - coordonnees[i+1][0] == 1 or coordonnees[i][0] - coordonnees[i+1][0] == -1: #olive/kaki
#                         terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 15

        

        


def mouvement(): # direction 3 = est, 2 = sud, 1 = ouest, 5 = nord
    global direction
    global deplacement
    direction = int(input(''))
    if direction == 3 or direction == 5: 
        deplacement = -1
    elif direction == 2 or direction == 1:
        deplacement = 1

def mettre_pomme():
    global pomme_manger
    if pomme_manger == True:
        x_pomme = randint(0, dimension - 1)
        y_pomme = randint(0, dimension - 1)
        if terrain_binaire[y_pomme][x_pomme] == 0:
            terrain_binaire[y_pomme][x_pomme] = 7
            pomme_manger = False

thread_mouvement_continuel = mouvement_continuel()
thread_mouvement_continuel.start()

while fin_du_jeu == False:
    mouvement()

thread_mouvement_continuel.join()
