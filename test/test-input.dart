main() {
  var stream = new StringInputStream();
  stream.onLine = () {
    var line = stream.readLine();
    if (line != null) {
      print(line);
    }
  };
}
