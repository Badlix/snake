import pygame
from pygame.locals import *
from random import *
from threading import Thread
import time

dimension_display = 800 #taille fenetre
dimension_grille = 15 #nb de case
dimension = int(dimension_display/dimension_grille) #dimension des cases

terrain_binaire = [] #0 = libre, 1 = tête du serpent, 2 = corp, 3 = queue
for i in range(dimension_grille):
    terrain_binaire.append([0] * dimension_grille)
fin_du_jeu = False
in_menu = True
quitter_score = True
pomme_manger = True
snake_longeur = 3
deplacement = -1
direction = 3
coordonnees = [] # [y, x]
aleatoire = randint(0,dimension_grille - 1) # defini la position de départ du serpent
coordonnees.append([aleatoire, 2])
coordonnees.append([aleatoire, 1])
coordonnees.append([aleatoire, 0])
def reset_value():
    global terrain_binaire
    global fin_du_jeu
    global pomme_manger
    global snake_longeur
    global deplacement
    global direction
    global coordonnees
    terrain_binaire = [] #0 = libre, 1 = tête du serpent, 2 = corp, 3 = queue
    for i in range(dimension_grille):
        terrain_binaire.append([0] * dimension_grille)
    fin_du_jeu = False
    pomme_manger = True
    snake_longeur = 3
    deplacement = -1
    direction = 3
    coordonnees = [] # [y, x]
    aleatoire = randint(0,dimension_grille - 1) # defini la position de départ du serpent
    coordonnees.append([aleatoire, 2])
    coordonnees.append([aleatoire, 1])
    coordonnees.append([aleatoire, 0])


logo_window = pygame.image.load('images/logo_window.png')

pygame.init()
display = pygame.display.set_mode((dimension_display,dimension_display+50)) 
pygame.display.set_caption('Snake')
pygame.display.set_icon(logo_window)
# fond = pygame.image.load('images/fond.png') #fond noir
# fond = pygame.transform.scale(fond, (dimension_display,dimension_display))
fond = pygame.image.load('images/fond2.png') #fond beige
fond = pygame.transform.scale(fond, (dimension_display,dimension_display))
logo = pygame.image.load('images/snake_logo.png')
logo = pygame.transform.scale(logo, (int(dimension_display/1.5),int(dimension_display/4)))
# display.blit(fond,(0,50))
font = pygame.font.Font('freesansbold.ttf', 32) #taille police
font_titre = pygame.font.Font('freesansbold.ttf', 60)
white = (255,255,255) #defini la couleur white
black = (0,0,0)
red = (223, 109, 20)
blue = (27, 1, 155)
tete_right = pygame.image.load('images/tete_right.png').convert_alpha()
tete_right = pygame.transform.scale(tete_right, (dimension,dimension)) #adapte la taille de l'image
tete_left = pygame.image.load('images/tete_left.png').convert_alpha()
tete_left = pygame.transform.scale(tete_left, (dimension,dimension))
tete_up = pygame.image.load('images/tete_up.png').convert_alpha()
tete_up = pygame.transform.scale(tete_up, (dimension,dimension))
tete_down = pygame.image.load('images/tete_down.png').convert_alpha()
tete_down = pygame.transform.scale(tete_down, (dimension,dimension))

corps_h = pygame.image.load('images/corps_h.png').convert_alpha()
corps_h = pygame.transform.scale(corps_h, (dimension, dimension))
corps_v = pygame.image.load('images/corps_v.png').convert_alpha()
corps_v = pygame.transform.scale(corps_v, (dimension,dimension))
corps_e_s = pygame.image.load('images/corps_e_s.png').convert_alpha()
corps_e_s = pygame.transform.scale(corps_e_s, (dimension,dimension))
corps_n_e = pygame.image.load('images/corps_n_e.png').convert_alpha()
corps_n_e = pygame.transform.scale(corps_n_e, (dimension,dimension))
corps_o_n = pygame.image.load('images/corps_o_n.png').convert_alpha()
corps_o_n = pygame.transform.scale(corps_o_n, (dimension,dimension))
corps_o_s = pygame.image.load('images/corps_o_s.png').convert_alpha()
corps_o_s = pygame.transform.scale(corps_o_s, (dimension,dimension))

queue_down = pygame.image.load('images/queue_down.png').convert_alpha()
queue_down = pygame.transform.scale(queue_down, (dimension,dimension))
queue_left = pygame.image.load('images/queue_left.png').convert_alpha()
queue_left = pygame.transform.scale(queue_left, (dimension,dimension))
queue_right = pygame.image.load('images/queue_right.png').convert_alpha()
queue_right = pygame.transform.scale(queue_right, (dimension,dimension))
queue_up = pygame.image.load('images/queue_up.png').convert_alpha()
queue_up = pygame.transform.scale(queue_up, (dimension,dimension))

pomme = pygame.image.load('images/pomme_test.png').convert_alpha()
pomme = pygame.transform.scale(pomme,(dimension,dimension))

you_lose_collision = pygame.image.load('images/you lose_collision.png')
you_lose_collision = pygame.transform.scale(you_lose_collision,(dimension_display,dimension_display))
you_lose_mur = pygame.image.load('images/you lose_mur.png')
you_lose_mur = pygame.transform.scale(you_lose_mur,(dimension_display,dimension_display))

vide = pygame.image.load('images/vide_test2.png')
vide = pygame.transform.scale(vide, (dimension,dimension))


def afficher():
    global direction
    y = 50
    x = 0
    fichier_score = open('score', 'r')
    list_score = []
    for line in fichier_score:
        line = line[:-1]
        list_score.append(line)
    fichier_score.close()
    high_score = list_score[1]
    score = font.render('score : ' + str(snake_longeur - 3),True, white) 
    high_score_txt = font.render('high score : ' + str(high_score),True,white)
    display.fill((0,0,0))
    display.blit(fond,(0,50))
    display.blit(high_score_txt, (20,15))
    display.blit(score, (dimension_display - 200,15))
    for ligne in terrain_binaire:
        for nombre in ligne:
            if nombre == 1:
                display.blit(tete_left, (x,y))
            elif nombre == 2:
                display.blit(tete_down, (x,y))
            elif nombre == 3:
                display.blit(tete_right, (x,y))
            elif nombre == 4:
                display.blit(tete_up, (x,y))
            elif nombre == 5:
                display.blit(corps_h, (x,y))
            elif nombre == 6:
                display.blit(corps_v, (x,y))
            elif nombre == 7:
                display.blit(pomme, (x,y))
            elif nombre == 8:
                display.blit(queue_right, (x,y))
            elif nombre == 9:
                display.blit(queue_down, (x,y))
            elif nombre == 10:
                display.blit(queue_up, (x,y))
            elif nombre == 11:
                display.blit(queue_left, (x,y))
            elif nombre == 12:
                display.blit(corps_e_s, (x,y))
            elif nombre == 13:
                display.blit(corps_o_s, (x,y))
            elif nombre == 14:
                display.blit(corps_o_n, (x,y))
            elif nombre == 15:
                display.blit(corps_n_e, (x,y))
            # elif nombre == 0:
            #     display.blit(vide, (x,y))
            x += (dimension)
        y += (dimension)
        x = 0

afficher()
pygame.display.flip()

class mouvement_continuel(Thread): #fait avancer le snake tt seul
    
    def __init__(self):
        Thread.__init__(self)
    
    def run(self):
        global direction
        global deplacement
        global snake_longeur
        global fin_du_jeu
        while fin_du_jeu == False:
            mettre_pomme()
            time.sleep(0.17)
            terrain_binaire[coordonnees[-1][0]][coordonnees[-1][1]] = 0
            if direction == 1 or direction == 3:
                if coordonnees[0][1] - deplacement > dimension_grille - 1 or coordonnees[0][1] - deplacement < 0:
                    display.blit(you_lose_mur, (0,50))
                    pygame.display.flip()
                    time.sleep(1.5)
                    fin_du_jeu = True
                    break
            if direction == 2 or direction == 5: 
                if coordonnees[0][0] + deplacement> dimension_grille - 1 or coordonnees[0][0] + deplacement< 0:
                    display.blit(you_lose_mur, (0,50))
                    pygame.display.flip()
                    time.sleep(1.5)
                    fin_du_jeu = True
                    break
            if snake_longeur == len(coordonnees): #si la pomme n'est pas mangé
                del coordonnees[-1]
            if direction == 1 or direction == 3:
                if terrain_binaire[coordonnees[0][0]][coordonnees[0][1] - deplacement] != 0 and terrain_binaire[coordonnees[0][0]][coordonnees[0][1] - deplacement] != 7: #regarde si il y a collision
                    display.blit(you_lose_collision, (0,50))
                    pygame.display.flip()
                    time.sleep(1.5)
                    fin_du_jeu = True
                    break
                coordonnees.insert(0, [coordonnees[0][0], coordonnees[0][1] - deplacement])
            if direction == 2 or direction == 5:
                if terrain_binaire[coordonnees[0][0] + deplacement][coordonnees[0][1]] != 0 and terrain_binaire[coordonnees[0][0] + deplacement][coordonnees[0][1]] != 7: #regarde si il y a collision
                    display.blit(you_lose_collision, (0,50))
                    pygame.display.flip()
                    time.sleep(1.5)
                    fin_du_jeu = True
                    break
                coordonnees.insert(0, [coordonnees[0][0] + deplacement, coordonnees[0][1]])
            update_terrain_binaire()
            afficher()
            pygame.display.flip()

def update_terrain_binaire():
    global pomme_manger
    global snake_longeur
    for i in range(len(coordonnees)):
        if i == 0:
            if terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] != 7:
                if direction == 1:
                    terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 1
                elif direction == 2:
                    terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 2
                elif direction == 3:
                    terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 3
                elif direction == 5:
                    terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 4
            else:
                # print('\n' + 'LA POMME A ETE MANGER' + '\n')
                pomme_manger = True
                snake_longeur += 1
                if direction == 1:
                    terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 1 #tete vers gauche
                elif direction == 2:
                    terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 2 #tete vers bas
                elif direction == 3:
                    terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 3 #tete vers droite
                elif direction == 5:
                    terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 4 #tete vers haut
        elif i == len(coordonnees) - 1:
            if coordonnees[i][0] - coordonnees[i-1][0] == 1: 
                terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 10 #queue vers haut
            if coordonnees[i][0] - coordonnees[i-1][0] == -1: 
                terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 9 #queue vers bas
            if coordonnees[i][1] - coordonnees[i-1][1] == 1: 
                terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 11 #queue vers gauche
            if coordonnees[i][1] - coordonnees[i-1][1] == -1: 
                terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 8 #queue vers droite
        else:
            if coordonnees[i-1][1] - coordonnees[i+1][1] == -2 or coordonnees[i-1][1] - coordonnees[i+1][1] == 2:
                terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 5 #corps horizontal
            elif coordonnees[i-1][0] - coordonnees[i+1][0] == -2 or coordonnees[i-1][0] - coordonnees[i+1][0] == 2:
                terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 6 #corps vertical
            elif coordonnees[i-1][1] - coordonnees[i+1][1] == -1:#bleu   ----> voir tableau libre office calc -> 'calcul virage corps serpent'
                if coordonnees[i-1][0] - coordonnees[i+1][0] == -1:#jaune
                    if coordonnees[i][0] - coordonnees[i+1][0] == 0:#gris
                        terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 15
                    if coordonnees[i][0] - coordonnees[i+1][0] == 1 or coordonnees[i][0] - coordonnees[i+1][0] == -1: #olive/kaki
                        terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 13
                if coordonnees[i-1][0] - coordonnees[i+1][0] == 1:#rouge
                    if coordonnees[i][0] - coordonnees[i+1][0] == 0:#gris
                        terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 12
                    if coordonnees[i][0] - coordonnees[i+1][0] == 1 or coordonnees[i][0] - coordonnees[i+1][0] == -1: #olive/kaki
                        terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 14
            elif coordonnees[i-1][1] - coordonnees[i+1][1] == 1:#vert
                if coordonnees[i-1][0] - coordonnees[i+1][0] == -1:#jaune
                    if coordonnees[i][0] - coordonnees[i+1][0] == 0:#gris
                        terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 14
                    if coordonnees[i][0] - coordonnees[i+1][0] == 1 or coordonnees[i][0] - coordonnees[i+1][0] == -1: #olive/kaki
                        terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 12
                if coordonnees[i-1][0] - coordonnees[i+1][0] == 1:#rouge
                    if coordonnees[i][0] - coordonnees[i+1][0] == 0:#gris
                        terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 13
                    if coordonnees[i][0] - coordonnees[i+1][0] == 1 or coordonnees[i][0] - coordonnees[i+1][0] == -1: #olive/kaki
                        terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 15


def mouvement(): # direction 3 = est, 2 = sud, 1 = ouest, 5 = nord
    global direction
    global deplacement
    direction = int(input(''))
    if direction == 3 or direction == 5: 
        deplacement = -1
    elif direction == 2 or direction == 1:
        deplacement = 1

def mettre_pomme():
    global pomme_manger
    if pomme_manger == True:
        x_pomme = randint(0, dimension_grille - 1)
        y_pomme = randint(0, dimension_grille - 1)
        if terrain_binaire[y_pomme][x_pomme] == 0:
            terrain_binaire[y_pomme][x_pomme] = 7
            pomme_manger = False

def ajouter_score():
    place = 9
    fichier_score = open('score', 'r')
    list_score = []
    for line in fichier_score:
        line = line[:-1]
        list_score.append(line)
    fichier_score.close()
    for i in range(0,18,2):
        if int(list_score[i+1]) < snake_longeur - 3:
            place -= 1
    if place < 9:
        name = ''
        enter_name = False
        while enter_name == False:
            for event in pygame.event.get():
                if event.type == KEYDOWN:
                    if event.unicode.isalpha():
                        name += event.unicode
                    elif event.key == K_BACKSPACE:
                        name = name[:-1]
                    elif event.key == K_RETURN:
                        enter_name = True
                elif event.type == QUIT:
                    return
            display.fill((0, 0, 0))
            block = font.render(name, True, (255, 255, 255))
            rect = block.get_rect()
            rect.center = display.get_rect().center
            txt = font.render('Taper votre nom :',True,white)
            display.blit(txt, (200,300))
            display.blit(block, rect)
            pygame.display.flip()
        fichier_score = open('score', 'w')
        place = place*2
        list_score[place] = name
        list_score[place+1] = snake_longeur - 3
        txt_a_ecrire = ''
        for element in list_score:
            element = str(element) + '\n'
            txt_a_ecrire = txt_a_ecrire + element
        fichier_score.write(txt_a_ecrire)
        fichier_score.close()

def main():
    global direction
    global deplacement
    reset_value()
    thread_mouvement_continuel = mouvement_continuel()
    thread_mouvement_continuel.start()

    while fin_du_jeu == False:
        for event in pygame.event.get():
            if event.type == QUIT:
                pygame.quit()
                exit
            if event.type == KEYDOWN:
                if event.key == K_RIGHT:
                    direction = 3
                    deplacement = -1
                if event.key == K_LEFT:
                    direction = 1
                    deplacement = 1
                if event.key == K_UP:
                    direction = 5
                    deplacement = -1
                if event.key == K_DOWN:
                    direction = 2
                    deplacement = 1

    thread_mouvement_continuel.join()
    ajouter_score()

def retour():
    global quitter_score
    quitter_score = True

def aff_score():
    global quitter_score
    quitter_score = False
    display.fill((blue))
    txt1 = font_titre.render('Nom',True,white)
    txt2 = font_titre.render('Score',True,white)
    display.blit(txt1,(100,100))
    display.blit(txt2,(500,100))   
    quitter = False
    fichier_score = open('score', 'r')
    list_score = []
    for line in fichier_score:
        print(line)
        line = line[:-1]
        print(line)
        list_score.append(line)
        print('\n')
    fichier_score.close()
    y1 = 200
    x1 = 100
    x2 = 500
    for i in range(0,20,2):
        txt = font.render(list_score[i],True, white)
        display.blit(txt, (x1,y1))
        txt = font.render(list_score[i+1],True,white)
        display.blit(txt, (x2,y1))
        y1 += 50
    while quitter_score == False:
        Button("<- Retour",25,15,250,50,'retour()',red)
        pygame.display.flip()
        for event in pygame.event.get():
            if event.type == QUIT:
                    pygame.quit()
                    exit

def printHey():
    print("Hey, that's work")

def printSlt():
    print("SALUT")

def Button(msg,x,y,w,h,fonction,color):
    pygame.draw.rect(display,color,(x,y,w,h))
    txt = font.render(msg,True,black) 
    display.blit(txt,(x+int(w/2)-int((len(msg)*22)/2),y + int(h/2)-16)) #x = largeur/2 - nb de lettre * taille police, y +hauteur/2 - moitié de la police
    mouse = pygame.mouse.get_pos() # = (x,y)
    click = pygame.mouse.get_pressed() # = (clic gauche, molette, clic droit) valeur = 0 ou 1
    if x + w > mouse[0] > x and y + h > mouse[1] > y and click[0] == 1:
        exec(fonction)
        time.sleep(0.5)
    
function_list = ['main()','aff_score()','pygame.quit()']

while in_menu == True:
    fin_du_jeu = False
    display.fill((127,127,127))
    display.blit(logo,(int(dimension_display/5),50))
    nb_fonction = 0
    Button('JOUER',300,350,250,100,function_list[nb_fonction],red)
    nb_fonction += 1
    Button('SCORE',300,475,250,100,function_list[nb_fonction],red)
    nb_fonction += 1
    Button('QUITTER',300,700,250,75,function_list[nb_fonction],red)
    pygame.display.flip()
    for event in pygame.event.get():
        if event.type == QUIT:
                pygame.quit()
                exit