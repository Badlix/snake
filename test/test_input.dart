import 'dart:async';
import "dart:io";

bool fin_du_jeu = false;

void main() {
  Timer.periodic(const Duration(seconds: 2), (timer) {
    if (!fin_du_jeu) {
      print('slt');
    } else {
      timer.cancel();
    }
  });
  stdout.writeln('Please enter your name? ');
  String yourName = stdin.readLineSync();
  stdout.writeln('Hello $yourName');

  stdout.writeln('Please enter your age? ');
  String yourAge = stdin.readLineSync();
  stdout.writeln('You are $yourAge years old');

  stdout.writeln('Hello $yourName, you are $yourAge years old today!');
}
