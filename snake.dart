//import 'dart:async';
import 'dart:io';
import 'dart:math';
import 'globales_dart.dart' as globals;

List terrain_binaire = []; //0=libre, 1=tête serpent, 2=corps, 3=queue, 5=pomme
int dimension = 10;
List coordonnees = []; // [y, x]
var aleatoire1 = new Random();
var aleatoire2 = aleatoire1.nextInt(dimension - 1);

afficher_terrain_binaire() {
  // inutil avec flutter
  print('-----------------------------');
  for (var i = 0; i < dimension; i++) {
    print(terrain_binaire[i]);
  }
}

update_terrain_binaire() {
  for (var i = 0; i < coordonnees.length; i++) {
    if (i == 0) {
      if (terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] != 7) {
        terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 1;
      } else {
        print('\n' + 'LA POMME EST MANGER' + '\n');
        globals.pomme_manger = true;
        terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 1;
        globals.snake_longueur += 1;
      }
    } else if (i == coordonnees.length - 1) {
      terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 3;
    } else {
      terrain_binaire[coordonnees[i][0]][coordonnees[i][1]] = 2;
    }
  }
}

mouvement() {
  //direction 1=ouest, 2=sud, 3=est, 5=nord (correpond au touche clavier)
  print('');
  var convert_direction = readLine();
  globals.direction = int.parse(convert_direction);
  print('mouvement() - ${globals.direction}');
  if (globals.direction == 3 || globals.direction == 5) {
    globals.deplacement = -1;
  } else if (globals.direction == 2 || globals.direction == 1) {
    globals.deplacement = 1;
  }
}

mettre_pomme() {
  if (globals.pomme_manger == true) {
    var aleatoire = new Random();
    int x_pomme = aleatoire.nextInt(dimension - 1);
    int y_pomme = aleatoire.nextInt(dimension - 1);
    if (terrain_binaire[y_pomme][x_pomme] == 0) {
      terrain_binaire[y_pomme][x_pomme] = 7;
      globals.pomme_manger = false;
    }
  }
}

thread() {
  mettre_pomme();
  terrain_binaire[coordonnees.last[0]][coordonnees.last[1]] = 0;
  if (globals.direction == 1 || globals.direction == 3) {
    if (coordonnees[0][1] - globals.deplacement > dimension - 1 || coordonnees[0][1] - globals.deplacement < 0) {
      print('\n' + 'PERDU' + '\n');
      globals.fin_du_jeu = true;
      return;
    }
  }
  if (globals.direction == 2 || globals.direction == 5) {
    if (coordonnees[0][0] + globals.deplacement > dimension - 1 || coordonnees[0][0] + globals.deplacement < 0) {
      print('\n' + 'PERDU' + '\n');
      globals.fin_du_jeu = true;
      return;
    }
  }
  if (globals.snake_longueur == coordonnees.length) {
    //si la pomme est mangé
    coordonnees.remove(coordonnees.last);
  }
  if (globals.direction == 1 || globals.direction == 3) {
    if (terrain_binaire[coordonnees[0][0]][coordonnees[0][1] - globals.deplacement] != 0 &&
        terrain_binaire[coordonnees[0][0]][coordonnees[0][1] - globals.deplacement] != 7) {
      //regarde si il y a collision horizontale
      print('\n' + 'PERDU' + '\n');
      globals.fin_du_jeu = true;
      return;
    }
    coordonnees.insert(0, [coordonnees[0][0], coordonnees[0][1] - globals.deplacement]);
  }
  if (globals.direction == 2 || globals.direction == 5) {
    if (terrain_binaire[coordonnees[0][0] + globals.deplacement][coordonnees[0][1]] != 0 &&
        terrain_binaire[coordonnees[0][0] + globals.deplacement][coordonnees[0][1]] != 7) {
      //regarde si il y a collision verticale
      print('\n' + 'PERDU' + '\n');
      globals.fin_du_jeu = true;
      return;
    }
    coordonnees.insert(0, [coordonnees[0][0] + globals.deplacement, coordonnees[0][1]]);
  }
  update_terrain_binaire();
  afficher_terrain_binaire();
}

main() {
  for (var i = 0; i < dimension; i++) {
    List ligne_terrain = [];
    for (var i = 0; i < dimension; i++) {
      ligne_terrain.add(0);
    }
    terrain_binaire.add(ligne_terrain);
  }
  coordonnees.add([aleatoire2, 2]);
  coordonnees.add([aleatoire2, 1]);
  coordonnees.add([aleatoire2, 0]);

  Timer.periodic(const Duration(seconds: 2), (timer) {
    if (!globals.fin_du_jeu) {
      thread();
    } else {
      timer.cancel();
    }
  });
  Timer.periodic(const Duration(milliseconds: 100), (timer) {
    if (!globals.fin_du_jeu) {
      mouvement();
    } else {
      timer.cancel();
    }
  });
}
